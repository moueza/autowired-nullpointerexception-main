package com.rebel.shop;

import org.springframework.beans.factory.annotation.Autowired;

import com.rebel.shop.persistence.jpa.service.RegionsServiceImpl;

//Main
/** https://stackoverrun.com/fr/q/7341253 */
public class JpaRepoTest {

	// ApplicationContext ctx;

	@Autowired
	RegionsServiceImpl regionsServiceImpl;

	public JpaRepoTest() {
		// ctx = new AnnotationConfigApplicationContext(DataConfig.class);
		// regionsServiceImpl = ctx.getBean("regionsServiceImpl",
		// RegionsServiceImpl.class);
	}

	public static void main(String[] args) {
		JpaRepoTest jpaRepoTest = new JpaRepoTest();
		jpaRepoTest.testService();
	}

	private void testService() {
		System.out.println(regionsServiceImpl.findById(3l).getName());
	}
}