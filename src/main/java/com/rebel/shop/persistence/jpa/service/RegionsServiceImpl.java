package com.rebel.shop.persistence.jpa.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.rebel.shop.persistence.jpa.repository.RegionsRepository;

@Service
public class RegionsServiceImpl implements RegionsService {

	@Resource
	private RegionsRepository regionsRepository;

	@Override
	public Regions findById(long id) {
		return regionsRepository.findOne(id);
	}
}