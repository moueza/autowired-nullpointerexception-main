package com.rebel.shop.persistence.jpa.service;

public interface RegionsService {
	public Regions findById(long id);
}