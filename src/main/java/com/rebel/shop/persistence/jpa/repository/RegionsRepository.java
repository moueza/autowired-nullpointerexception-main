package com.rebel.shop.persistence.jpa.repository;

import com.rebel.shop.persistence.jpa.service.*;

public interface RegionsRepository extends JpaRepository<Regions, Long> {

	Regions findOne(long id);
}